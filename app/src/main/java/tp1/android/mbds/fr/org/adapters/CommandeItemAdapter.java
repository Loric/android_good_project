package tp1.android.mbds.fr.org.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.List;

import tp1.android.mbds.fr.org.Singleton;
import tp1.android.mbds.fr.org.activities.ProductActivity;
import tp1.android.mbds.fr.org.activities.R;
import tp1.android.mbds.fr.org.activities.TakeCommandActivity2;
import tp1.android.mbds.fr.org.odt.Product;

/**
 * Created by loric on 13/12/2015.
 */
public class CommandeItemAdapter extends BaseAdapter {

    private Context context;
    public List<Product> commande;

    public CommandeItemAdapter(Context context, List<Product> commande) {
        this.context = context;
        this.commande = new ArrayList<>();
        this.commande.addAll(commande);
    }

    @Override
    public int getCount() {
        return commande.size();
    }

    @Override
    public Object getItem(int position) {
        return commande.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        CommandeViewHolder viewHolder = new CommandeViewHolder();
        if(v == null){
            v = View.inflate(context, R.layout.commande, null);
            viewHolder.name= (TextView)v.findViewById(R.id.commande_name);
            viewHolder.img= (ImageView)v.findViewById(R.id.commande_picture);
            viewHolder.type= (TextView)v.findViewById(R.id.commande_type);
            viewHolder.seeAll= (TextView)v.findViewById(R.id.commande_type_see_all);
            v.setTag(viewHolder);
        }
        else{
            viewHolder = (CommandeViewHolder) v.getTag();
        }
        final Product product = commande.get(position);
        String name = product.getName();
        String picture = product.getPicture();
        String type = product.getType();
        String seeAll = "";
        if(type.equals("Dessert")||type.equals("Plat") || type.equals("Appéritif")){
            seeAll = "voir tous les "+type.toLowerCase()+"s";
        } else if(type.equals("Entrée")){
            seeAll = "voir toutes les "+type.toLowerCase()+"s";
        }
        else{
            seeAll = "voir toutes les NONFAIT -.-'";
        }

        viewHolder.name.setText(name);
        viewHolder.type.setText(type);
        viewHolder.seeAll.setText(seeAll);

        AQuery aq = new AQuery(v);
        aq.id(viewHolder.img).image(aq.getCachedImage(picture)).width(120);


        //*
        TextView productSeeButton = (TextView) v.findViewById(R.id.commande_see_product_button);

        productSeeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("product", product);
                context.startActivity(intent);
            }

        });


        TextView productDeleteButton = (TextView) v.findViewById(R.id.product_add_button);

        productDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                commande.remove(product);
                Singleton.getInstance().getCommande().remove(product);
                CommandeItemAdapter.this.notifyDataSetChanged();
                if(commande.isEmpty()){
                    Intent intent = new Intent(context, TakeCommandActivity2.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Toast.makeText(context, "Command is empty", Toast.LENGTH_LONG).show();
                    context.startActivity(intent);
                }
            }

        });
        //*/

        return v;
    }



    class CommandeViewHolder{
        TextView type;
        TextView name;
        ImageView img;
        TextView seeAll;

    }

}
