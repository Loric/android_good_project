package tp1.android.mbds.fr.org.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import tp1.android.mbds.fr.org.adapters.PersonItemAdapter;
import tp1.android.mbds.fr.org.api.config.ApiAdresses;
import tp1.android.mbds.fr.org.odt.Person;

public class AllPersonActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    ListView listServeur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_person);

        listServeur = (ListView) findViewById(R.id.listViewPerson);
        new AsynGetAllServeurs().execute();
/*        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }



    class AsynGetAllServeurs extends AsyncTask<String, Void, JSONArray> {

        private URI url;

        AsynGetAllServeurs() {
            try {
                //url = new URI("http://92.243.14.22/person/");
                url = new URI(ApiAdresses.getPersonUrl());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected JSONArray doInBackground(String... params) {
            try{
                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(url);

                // add header

                get.setHeader("Content-Type", "application/json");

                HttpResponse response = client.execute(get);
                Log.w("toto", "\nSending 'GET' request to URL : " + url);
                Log.w("toto", "Response Code : " +
                        response.getStatusLine().getStatusCode());

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));


                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                Log.w("RESULTAT !!!!! :: ", result.toString());
                JSONArray jsonArray = new JSONArray(result.toString());

                return jsonArray;
            } catch (Exception e){

            }
            return null;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            showProgressDialog(true);
        }

        @Override
        protected void onPostExecute(JSONArray response) {
            super.onPostExecute(response);
            showProgressDialog(false);
            //ListView lst = (ListView)findViewById(R.id.listView);
            List<Person> persons = new ArrayList<>();
            try {
                JSONArray array = response;
                for(int i = 0; i<array.length(); i++){
                    JSONObject ob = array.getJSONObject(i);
                    Person p = new Person();
                    try {
                        p.setNom(ob.getString("nom"));
                        p.setPrenom(ob.getString("prenom"));
                        p.setId(ob.getString("id"));
                        p.setConnected(Boolean.parseBoolean(ob.getString("connected")));
                        persons.add(p);
                    }catch (Exception e){
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            PersonItemAdapter adapter = new PersonItemAdapter(getApplicationContext(), persons);
            listServeur.setAdapter(adapter);
        }
    }

    public void menu(View view){
        Intent intent = new Intent(AllPersonActivity.this, MenuActivity.class);
        startActivity(intent);
    }

    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(this.getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }
}
